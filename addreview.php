
<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

function addReviewForm($xmlFileName) {
	if (isset($_SESSION['authenticatedUser'])) {
		echo "<form action='addreview.php' method='POST'>
		<input type='hidden' name='xmlFileName' value='$xmlFileName'>
		<select name='rating'>
		<option value='1'>1</option>
		<option value='2'>2</option>
		<option value='3'>3</option>
		<option value='4'>4</option>
		<option value='5'>5</option>
		</select>
		<input type='submit' value='Add Review'>";

		echo "</form>"; }
	}

	if(isset($_POST['xmlFileName'])){
		$test = 'reviews/' .  $_POST['xmlFileName'];
		echo $test;
		$reviews = simplexml_load_file($test);

		$newReview = $reviews->addChild('review');

		$newReview->addChild('user', $_SESSION['name']);
		$newReview->addChild('rating', $_POST['rating']);



		$reviews->saveXML($test);

		header('Location: '. $_SESSION['lastPage'] ); 
		exit;
	}

