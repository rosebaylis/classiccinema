<?php
$scriptList = array('jquery-1.11.1.min.js', 'cookies.js');
include("noaccess/header.php");
include("validationFunctions.php");
?>
<div id="main">

	<?php

	function showProducts(){
		if(isset($_COOKIE['cart'])){

	//echo'yes there is a cookie';
			$cart = json_decode($_COOKIE['cart']);	


			?>
			<div id="cart">
				<table>
					<tr><th>Title (Year)</th><th>Price</th></tr>

					<?php
					foreach($cart as $cartitem){

						echo "<tr>
						<td>" . $cartitem->title . "</td>
						<td class = \"money\">" . $cartitem->price . "</td>

					</tr>";

				}//end for each ?> 
			</table>
		</div>
		<?php
		}//end if the cookie is set
	}//end function
	?>

	<?php
	if(isset($_SESSION['authenticatedUser'])){ 

		if(!validatecheckout()===false){ ?>

		<p>Success form validated with no errors </p>
		<?php

		showProducts();
		$orders = simplexml_load_file('orders.xml');
		$newOrder = $orders->addChild('order');
		$delivery = $newOrder->addChild('delivery');
		$delivery->addChild('name', $_POST['name']);
		$delivery->addChild('email', $_POST['email']);
		$delivery->addChild('address1', $_POST['address1']);
		$delivery->addChild('address2', $_POST['address2']);
		$delivery->addChild('city', $_POST['city']);
		$delivery->addChild('postcode', $_POST['postcode']);

		$cart = json_decode($_COOKIE['cart']);	
		foreach($cart as $cartitem){
			$item=$newOrder->addChild('item');
			$item->addChild('title', $cartitem->title );
			$item->addChild('price', $cartitem->price );
		}
		$orders->saveXML('orders.xml');

		setcookie('cart', '', time()-3600, '/');
		unset($_COOKIE['cart']);

		
	}

}else{
	echo "<p>You need to be logged in to checkout. Please log in or <a href=\"register.php\">register</a>.";

}

?>
</div>	

<?php
include("noaccess/footer.php"); 
?>
</body>
</html>



