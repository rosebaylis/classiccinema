<!DOCTYPE html>

<html>
<?php
$scriptList = array('jquery-1.11.1.min.js', 'showHide.js', 'cookies.js', 'checkout.js','checkoutValidation.js' );
$currentPage = basename($_SERVER['PHP_SELF']);
include('noaccess/header.php');
include("validationFunctions.php");
?>
<div id = "main">

  <?php  
  if(isset($_SESSION['authenticatedUser'])){ 
    ?>

    <?php
    $xml = simplexml_load_file("orders.xml");
    $orders = $xml->xpath('//order');
    $items = $xml->xpath('//item');
    foreach($orders as $order){
     if($order->delivery->email == $_SESSION['authenticatedUser'] OR $_SESSION['role']=='admin'){
        ?>
        <h3>Order</h3>
        <table>
    <tr>
      <td>Name: </td>
      <td> <?php print $order->delivery->name ?></td>
    </tr>
    <tr>
      <td>Email:  </td>
      <td>  <?php print $order->delivery->email ?></td>
    </tr>
    <tr>
      <td>Address 1:</td>
      <td>  <?php print $order->delivery->address1 ?></td>
    </tr>
    <tr>
      <td>Address 2:</td>
      <td>  <?php print $order->delivery->address2 ?></td>
    </tr>
    <tr>
      <td>City:</td>
      <td>  <?php print $order->delivery->city ?></td>
    </tr>
    <tr>
      <td>Postcode:</td>
      <td>  <?php print $order->delivery->postcode ?></td>
    </tr>
  </table>
  <?php 
  foreach($items as $item){
    ?>

    <h4>Item</h4>
    <table>
      <tr>
        <td>Title: </td>
        <td> <?php print $item->title ?></td>
      </tr>
      <tr>
        <td>Price:  </td>
        <td>  <?php print $item->price ?></td>
      </tr>

    </table>
       

     <?php }
      } else{
        echo "You have not made any orders";
      }
    } 
  }else{
  header('Location: index.php' ); 
  exit;

}
  
 /* if(isset($_SESSION['role']) && $_SESSION['role']=='admin'){ 
    
  $items = $xml->xpath('//item');
  $orders = $xml->xpath('//order');
  foreach($orders as $order){
  ?>
  <h3>Order</h3>
  <table>
    <tr>
      <td>Name:	</td>
      <td> <?php print $order->delivery->name ?></td>
    </tr>
    <tr>
      <td>Email:  </td>
      <td>  <?php print $order->delivery->email ?></td>
    </tr>
    <tr>
      <td>Address 1:</td>
      <td>  <?php print $order->delivery->address1 ?></td>
    </tr>
    <tr>
      <td>Address 2:</td>
      <td>  <?php print $order->delivery->address2 ?></td>
    </tr>
    <tr>
      <td>City:</td>
      <td>  <?php print $order->delivery->city ?></td>
    </tr>
    <tr>
      <td>Postcode:</td>
      <td>  <?php print $order->delivery->postcode ?></td>
    </tr>
  </table>
  <?php 
  foreach($items as $item){
    ?>

    <h4>Item</h4>
    <table>
      <tr>
        <td>Title: </td>
        <td> <?php print $item->title ?></td>
      </tr>
      <tr>
        <td>Price:  </td>
        <td>  <?php print $item->price ?></td>
      </tr>

    </table>

  <?php 
    }; 

  };*/


?>
</div>

<?php
include "noaccess/footer.php" ?>

</body>
</html>
