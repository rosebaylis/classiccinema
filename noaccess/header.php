<?php
if (session_id() === "") { session_start();
}
$_SESSION['lastPage'] = $_SERVER['PHP_SELF'];
/**
 * Created by PhpStorm.
 * User: rbaylis
 * Date: 9/4/14
 * Time: 1:18 PM
 *
 */
echo "<head>";


if (isset($scriptList) && is_array($scriptList)) {
    foreach ($scriptList as $script) {
        echo "<script src='js/$script'></script>";
    }
}

?>
<title>Classic Cinema</title>
<link rel="stylesheet" href="style.css">
<meta charset="utf-8">

</head>

<body>
    <header>
        <h1>Classic Cinema</h1>
        <?php 
        if(!isset($_SESSION['authenticatedUser'])){ ?>

        <div id="login">
            <form id="loginForm" action="login.php" method="post">
                <label for="loginEmail">Email: </label>
                <input type="text" name="loginEmail" id="loginUser"><br>
                <label for="loginPassword">Password: </label>
                <input type="password" name="loginPassword" id="loginPassword"><br>
                <input type="submit" id="loginSubmit" value="Login">
            </form>
            <?php 

//            echo $_SESSION['nouser']; 
  //          unset($_SESSION['nouser']);

            if (isset($_SESSION['nouser'])){
                echo $_SESSION['nouser']; 
                unset($_SESSION['nouser']);
            }

        ?>

    </div>

    <?php }else{ ?>

    <div id="logout">
        <p>Welcome! </p></p>You are logged in as 
        <?php echo $_SESSION['fname'] . ' ' .$_SESSION['lname'];
        ?> 
        <span id="logoutUser"></span></p>
        <form id="logoutForm" action="logout.php" method="post" >
            <input type="submit" id="logoutSubmit" value="Logout">
        </form>

    </div>

    <?php } ?>



</header>



<nav>

    <ul> 
        <?php
        $currentPage = basename($_SERVER['PHP_SELF']);

        if ($currentPage === 'index.php') {
            echo "<li> Home";
        } else {
            echo "<li> <a href='index.php'>Home</a>";
        }

        if(!isset($_SESSION['authenticatedUser'])){ 

            if ($currentPage === 'register.php') {
                echo "<li> Register";
            } else {
                echo "<li> <a href='register.php'>Register</a>";
            }
        }

        if ($currentPage === 'classic.php') {
            echo "<li> Classics";
        } else {
            echo "<li> <a href='classic.php'>Classic</a>";
        }

        if ($currentPage === 'scifi.php') {
            echo "<li> Science Fiction and Horror";
        } else {
            echo "<li> <a href='scifi.php'>Science Fiction and Horror</a>";
        }
        if ($currentPage === 'hitchcock.php') {
            echo "<li> Alfred Hitchcock";
        } else {
            echo "<li> <a href='hitchcock.php'>Alfred Hitchcock</a>";
        }
        if(isset($_SESSION['authenticatedUser'])){ 

            if ($currentPage === 'checkout.php') {
                echo "<li> Checkout";
            } else {
                echo "<li> <a href='checkout.php'>Checkout</a>";
            }
            if ($currentPage === 'orders.php') {
                echo "<li> Orders";
            } else {
                echo "<li> <a href='orders.php'>Orders</a>";
            }
        }

        echo "</ul></nav>";
