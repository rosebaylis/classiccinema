<!DOCTYPE html>

<html>
<?php
$scriptList = array('jquery-1.11.1.min.js', 'showHide.js', 'cookies.js', 'checkout.js','checkoutValidation.js' );
$currentPage = basename($_SERVER['PHP_SELF']);
include('noaccess/header.php');
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
include("validationFunctions.php");


?>


<div id="main">
    <!-- Content is JavaScript driven -->
    <h3>Shopping Cart Contents</h3>
    <div id="cart"></div>
    <div id="errors"></div>

<?php  if(isset($_SESSION['authenticatedUser'])){ 
?>
    <form id="checkoutForm" method='POST' action='validateCheckout.php' novalidate>
        <fieldset>
            <legend>Delivery Details:</legend>
            <p>
                <label for="deliveryName">Deliver to:</label>
                <input type="text" name="name" id="deliveryName"
                <?php
                if(isset($_SESSION['name'])){
                    setFieldValue('name');
                }                          	 
                ?> required>

            </p>
            <p>
                <label for="deliveryEmail">Email:</label>
                <input type="email" name="email" id="deliveryEmail"
                <?php
                if(isset($_SESSION['email'])){
                    setFieldValue('email');
                }
                ?>>
            </p>
            <p>
                <label for="deliveryAddress1">Address:</label>
                <input type="text" name="address1" id="deliveryAddress1"      <?php
                if(isset($_SESSION['address1'])){
                    setFieldValue('address1');
                }
                ?> required>
            </p>
            <p>
                <label for="deliveryAddress2"></label>
                <input type="text" name="address2" id="deliveryAddress2"      <?php
                if(isset($_SESSION['address2'])){
                    setFieldValue('address2');
                }
                ?>>
            </p>
            <p>
                <label for="deliveryCity">City:</label>
                <input type="text" name="city" id="deliveryCity"
                <?php
                if(isset($_SESSION['city'])){
                    setFieldValue('city');
                }
                ?>	
                required>
            </p>
            <p>
                <label for="deliveryPostcode">Postcode:</label>
                <input type="text" name="postcode" id="deliveryPostcode" maxlength="4"      <?php
                if(isset($_SESSION['postcode'])){
                    setFieldValue('postcode');
                }
                ?> required class="short">
            </p>
        </fieldset>

        <fieldset>
            <p>
                <label for="cardType">Card type:</label>


                <select name="cardType" id="cardType"> 
                    <option value="amex" <?php 
                    if(isset($_SESSION['cardType'])){ 
                        $cardType=$_SESSION['cardType'];                 
                        if("amex"===$cardType){
                         echo "selected";}
                     }?>  >American Express
                 </option>

                 <option value="mcard" <?php 
                 if(isset($_SESSION['cardType'])){ 
                    $cardType=$_SESSION['cardType']; 
                    if("mcard"===$cardType){
                        echo "selected"; } 
                    }?> >Master Card
                </option>

                <option value="visa" <?php 
                if(isset($_SESSION['cardType'])){ 
                    $cardType=$_SESSION['cardType']; 
                    if("visa"===$cardType){ 
                        echo "selected"; } 
                    }?> >Visa
                </option>
            </select> 
        </p> 

        <p>
            <label for="cardNumber">Card number:</label> 
            <input type="text" name="cardNumber" id="cardNumber" maxlength="16" required 
            <?php if(isset($_SESSION['cardNumber'])){ 
                $cardNumber=$_SESSION['cardNumber']; 
                echo "value='$cardNumber'"; } ?> > 
            </p>

            <p>
             <label for="cardMonth">Expiry Month:</label>
             <select name="cardMonth" id="cardMonth"> 
                 <?php $options=array(1,2,3,4,5,6,7,8,9,10,11,12); 
                 foreach($options as $option){ 
                    echo('<option value="'.$option.'">'.$option.'</option>');
                    if(isset($_SESSION['cardMonth'])){
                     $cardMonth=$_SESSION['cardMonth']; 
                     if($option==$cardMonth){ echo('<option value="'.$option.'"selected>'.$option.'</option>'); }
                 } 
             } ?>
         </p>
     </select>

     <p>
         <label for="cardYear">Card year:</label> 
         <select name="cardYear" id="cardYear"> 
            <?php $yearOptions=array(2014, 2015,2016,2017,2018,2019,2020,2021);
            foreach($yearOptions as $year){ 
                echo('<option value="'.$year.'">'.$year.'</option>');
                if(isset($_SESSION['cardYear'])){ 
                    $cardYear=$_SESSION['cardYear']; 
                    if($year==$cardYear){ 
                        echo('<option value="'.$year.'"selected>'.$year.'</option>');

                    }
                }
            } ?>
        </p>
    </select>

    <p>
    <label for="cardValidation">CVC:</label> 
       <input type="text" class= "short" name="cardValidation" id="cardValidation" maxlength="4" required 
         <?php if(isset($_SESSION['cardValidation'])){
                    setFieldValue('cardValidation');
                }?>
    ></p>

</fieldset>
<input type="submit">
</form>
<?php }else{
echo "<p>You need to be logged in to checkout. Please log in or <a href=\"register.php\">register</a>.";

    } ?>


</div>

<?php include "noaccess/footer.php" ?>

</body>
</html>
