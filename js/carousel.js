/**
 * Image Carousel for the front page of the Classic Cinema site.
 *
 * Created by: Steven Mills, 09/04/2014
 * Last Modified by: Steven Mills 04/08/2014
 */

/*jslint browser: true, devel: true */

/**
 * Module pattern for Carousel functions
 */
var Carousel = (function () {
    "use strict";

    var movieList, movieIndex, pub;

    // Public interface
    pub = {};

    /**
     * Constructor function for MovieCateogry objects
     *
     * These objects represent movie categories as shown on the front page.
     * Note that as a constructor this function does not return a value.
     * Instead it should be called using the form: new MovieCategory(...);
     *
     * @param title The title of the category to display
     * @param image The image to display for the category
     * @param page The category page to link to
     */
    function MovieCategory(title, image, page) {
        this.title = title;
        this.image = image;
        this.page = page;
        this.makeHTML = function () {
            var html;
            html = "<div id = \"film\">";
            html += "<a href='" + page + "'>";
            html += "<img src='" + image + "'>";
            html += "<br>" + title;
            html += "</img>";
            html += "</a>";
            html += "</div>";
            return html;
        };
    }

    /**
     * Update the carousel to the next category
     */
    function nextCategory() {
        var carousel, film;
        carousel = $("#carousel");
        carousel.html(movieList[movieIndex].makeHTML());
        film = $("#film");
        //  $("#film").css({opacity:0});
        //   $("#film").animate({opacity:1},1500);
        //   $("#film").animate({opacity:0},1500);


        $("#main").css({"height": "320px"});

        film.animate({opacity: 0}, 2500);
        film.css({"float": "right"});
        film.css({"width": "250"});
        carousel.css({"width": "0"}, 1500);
        carousel.animate({"width": "580"}, 1500);


        movieIndex += 1;
        if (movieIndex >= movieList.length) {
            movieIndex = 0;
        }
    }

    /**
     * Setup function for the carousel
     *
     * Creates a list of MovieCategory objects, and starts the timer
     */
    pub.setup = function () {
        movieList = [];
        movieList.push(new MovieCategory("Classics", "images/Metropolis.jpg", "classic.php"));
        movieList.push(new MovieCategory("Science Fiction and Horror", "images/Plan_9_from_Outer_Space.jpg", "scifi.php"));
        movieList.push(new MovieCategory("Alfred Hitchcock", "images/Vertigo.jpg", "hitchcock.php"));
        movieIndex = 0;
        nextCategory();
        setInterval(nextCategory, 2000);
    };

    // Expose public interface
    return pub;
}());


$(document).ready(Carousel.setup);
