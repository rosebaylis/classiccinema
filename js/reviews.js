/**
 * Created by rbaylis on 8/7/14.
 */
/*jslint browser: true, devel: true, sloppy: true, white: true */
var Reviews = (function () {
    var pub = {};



    function parseReviews(data, target) {
        var reviewNode;
        reviewNode = $(data).find("review");

        if(reviewNode.length){
        $(reviewNode).each(function () {
            var rating, user;
            rating = $(this).find("rating")[0].textContent;
            user = $(this).find("user")[0].textContent;
            $(target).append("<dl>");
            $(target).append("<dt>" + user + ": " + "</dt><dd>" + rating + "</dd>");
            $(target).append("</dl>");
        });
    }else{
            $(target).html("Sorry, there are no reviews for this film");

        }
    }
    function showReviews() {
        var target, xmlSource;
        target = $(this).parent().find(".review")[0];
        xmlSource = $(this).parent().find("img")[0].src;
        //only if there is nothing in the div do we want to populate it
        if ($(target).is(':empty')) {
            xmlSource = xmlSource.replace("images", "reviews");
            xmlSource = xmlSource.replace(".jpg", ".xml");
            $.ajax({
                type: "GET",
                url: xmlSource,
                cache: false,
                success: function (data) {
                    parseReviews(data, target);
                },
                error: function() {
                    $(target).html("Sorry, there are no reviews for this film");
                }
            });

        }
    }

    pub.setup = function () {
        $(".film").append("<input type=\"button\" class=\"showReviews\" value=\"Show Reviews\">" +
            "<div class=\"review\"></div>");

        //when clicked show the info
        $(".showReviews").click(showReviews);
       //initally set to hidden
        $(".showReviews").parent().find(".review").hide();
       //shpw and hide when clicked
        $(".showReviews").click(function () {
            $(this).parent().find(".review").toggle();
        });
    };

    return pub;

}());

$(document).ready(Reviews.setup);