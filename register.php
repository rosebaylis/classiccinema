<!DOCTYPE html>

<html>
<?php
$scriptList = array('jquery-1.11.1.min.js', 'showHide.js', 'cookies.js', 'checkout.js','checkoutValidation.js' );
$currentPage = basename($_SERVER['PHP_SELF']);
include('noaccess/header.php');
session_start();
include("validationFunctions.php");
?>


<div id="main">
    <h3>Register</h3>
    <form id="registerForm" method='POST' action='register.php'>
        <fieldset>
            <legend>Your Details:</legend>
            <p>
                <label for="fname">First Name:</label>
                <input type="text" name="fname" id="fname"  <?php
                if(isset($_SESSION['fname'])){
                    setFieldValue('fname');
                }
                ?>   required>
            </p>

            <p>
                <label for="lname">Last Name:</label>
                <input type="text" name="lname" id="lname"  <?php
                if(isset($_SESSION['lname'])){
                    setFieldValue('lname');
                }
                ?>   required>
            </p>
            <p>
                <label for="email">Email:</label>
                <input type="email" name="email" id="email"    <?php
                if(isset($_SESSION['email'])){
                    setFieldValue('email');
                }
                ?>   required>
            </p>
            <p>
                <label for="password">Password:</label>
                <input type="password" name="password" id="password" required>
            </p>
            <p>
                <label for="re-password">Retype password:</label>
                <input type="password" name="re-password" id="re-password" required>
            </p>


        </fieldset>
        <input type="submit" value="Submit">
    </form>

    <?php
/*
if(isPassword('12Ff')===true){
echo"hello";
}*/

setSession('fname');
setSession('lname');
setSession('email');

function validateRegisterForm(){


    $count=0;
    if(isset($_POST['password']) and (preg_match('/[A-Z]+/', $_POST['password']) && 
        preg_match('/[a-z]+/', $_POST['password']) && 
        preg_match('/[\d!$%^&]+/', $_POST['password'])) === false ){
      echo "<p>Password must have at least one uppercase, one lowercase and one number</p>";
  $count++;

}




if(isset($_POST['password']) and strlen(trim($_POST['password']))<8 ){
    echo "<p>Password must be at least 8 characters</p>";
    $count++;
}


if(isset($_POST['password']) and isset($_POST['re-password']) and $_POST['password']!==$_POST['re-password']){
    echo "<p>Passwords must match<p>";
    $count++;
}


if(isset($_POST['email']) and strlen(trim( $_POST['email']))===0){
    echo "<p>Email must be entered</p>";
    $count++;
}

if($count>0){
    return false;
}else{
    return true;
}

}

if(validateRegisterForm()===true){

    include "noaccess/db_conn.php";
    if ($conn->connect_errno) {
      echo "Something went wrong connecting";
  }

  if(isset($_POST['email'])){
    $query = "SELECT * FROM users WHERE email = '$email'";
    $result = $conn->query($query);
    if ($result->num_rows === 0) {
        $email=$conn->real_escape_string($_POST['email']);
        $fname = $conn->real_escape_string($_POST['fname']);
        $lname= $conn->real_escape_string($_POST['lname']);
        $pwd = $conn->real_escape_string($_POST['password']);

        $query = "insert into users (fname,lname,email,password,role) values ('$fname','$lname','$email', '$pwd', 'user' )";
        
       
        $conn->query($query);
         echo "New user ".$fname . " " . $lname . " added.";
        $_SESSION['authenticatedUser'] = $email;
        header('Location: index.php'); 
exit;



    } else {
        echo " Problem -- email is already taken";
    }
    $result->free();
    $conn->close();

}

}

?>
</div>
<?php include "noaccess/footer.php" ?>
</body>
</html>
