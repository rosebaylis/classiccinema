<!DOCTYPE html>

<html>
        <?php
        $scriptList = array('jquery-1.11.1.min.js', 'carousel.js');
        $currentPage = basename($_SERVER['PHP_SELF']);
        include('noaccess/header.php');
        ?>

    <div id="main">
            <p>
                Welcome to Classic Cinema, your online store for classic film.
            </p>
            <div id="carousel">
                <!-- Content generated by JavaScript in carousel.js -->
            </div>
        </div>

        <?php include 'noaccess/footer.php';?>


    </body>
</html>
